To get setup:

1. Clone the project via `git clone https://gitlab.com/nicholas.tower/interview.git`
2. Change directory into the project: `cd interview`
3. Install dependencies: `npm install`

To run the app:

1. Start the development server `npm run server`
2. In another terminal window, start the client `npm start`
